import string

# Definicja list znaków uwzględnianych w szyfrowaniu i nieuwzględnianych w szyfrowaniu.
POLSKIE = ['ą','Ą','ć','Ć','ę','Ę','ó','Ó','ł','Ł','ż','Ż','ź','Ź','ś','Ś','ń','Ń'] # Defnicja polskich znaków, ponieważ nie ma wbudowanych w moduł string
LITERY = list(string.ascii_letters) + POLSKIE
# Lista znaków interpunkcyjnych z uwzględnieniem polskich cudzysłowów
INTERPUNKCJA = string.punctuation + "„" + '”' + '°' + '–'
# Znaki białe
BIALE = string.whitespace
# Liczby
LICZBY = string.digits

# Przesunięcie w szyfrze
przesuniecie = 10

# Funkcja zamieniająca literę niezaszyfrowaną na literą zaszyfrowaną odległą o dane przesunięcie
def zaszyfruj_litere(litera_input,
                     przesuniecie):
    return LITERY[(LITERY.index(litera_input) + przesuniecie) % len(LITERY)]

# Funkcja zamieniająca literę zaszyfrowaną na literą niezaszyfrowaną odległą o dane przesunięcie
def rozszyfruj_litere(litera_input,
                      przesuniecie):
    return LITERY[(LITERY.index(litera_input) - przesuniecie) % len(LITERY)]

# Funkcja szyfrująca plik
def szyfruj(plik_rozszyfrowany, plik_zaszyfrowany):
    # Przeczytaj linie w pliku niezaszyfrowanym
    for line in plik_rozszyfrowany.readlines():
        # Prealokacja listy jako linię wyjściową
        linia_zaszyfrowana = []
        # Iterowanie po znakach w lini
        for char in line:
            # sprawdzenie czy znak jest biały/interpukcyjny/liczba
            if char in BIALE or \
               char in INTERPUNKCJA or \
               char in LICZBY:
                # Przepisanie znaku
                linia_zaszyfrowana.append(char)
            else:
                # Zaszyfruj jeśli jest literą
                linia_zaszyfrowana.append(zaszyfruj_litere(char, przesuniecie))
        # Scalanie listy liter do stringa
        linia_zaszyfrowana = ''.join(linia_zaszyfrowana)
        # Zapisz stringa w pliku wyjsciowym
        plik_zaszyfrowany.write(''.join(linia_zaszyfrowana))
    return

def rozszyfruj(plik_zaszyfrowany, plik_rozszyfrowany):
    # Iteracja po liniach pliku zaszyfrowanym
    for line in plik_zaszyfrowany.readlines():
        # Alokacja listy znaków wyjsciowych
        linia_rozszyfrowana = []
        # Iteracja po znakach w lini
        for char in line:
            # sprawdzenie czy znak jest biały/interpukcyjny/liczba
            if char in BIALE or \
               char in INTERPUNKCJA or \
               char in LICZBY:
                # Przepisz znak inny niz litera
                linia_rozszyfrowana.append(char)
            else:
                # Rozszyfruj literę
                linia_rozszyfrowana.append(rozszyfruj_litere(char, przesuniecie))
        # Scal listę znaków do stringa
        linia_rozszyfrowana = ''.join(linia_rozszyfrowana)
        # Zapisz stringa w pliku wyjsciowym
        plik_rozszyfrowany.write(linia_rozszyfrowana)
    return

# Otwórz plik wejściowy
plik_input = open('wyjscie.txt')
# Stwórz plik wyjściowy z prawami zapisu. Jeśli plik istnieje zostanie nadpisany
plik_output = open('rozszyfr.txt', 'w')

# Wykonaj szyfrowanie
rozszyfruj(plik_input,plik_output)

# Zamknij plik wejściowy
plik_input.close()
# Zamknij plik wyjściowy
plik_output.close()